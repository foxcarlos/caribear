{
    'name': "Caribear",
    'summary': """
        Theme Caribear""",
    'author': "Carlos Garcia",
    'website': "http://www.carlosgarciadiaz.com.ar",
    'category': 'website',
    'license': 'AGPL-3',
    'version': '13.0.1.0.0',
    'depends': ['base'],
    'data': ['views/snippets.xml', 'views/assets.xml'],
    'demo': [
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
